import './assets/main.css'
import './assets/app.scss'

import { createApp } from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'

import '@mdi/font/css/materialdesignicons.css'

import 'vuetify/styles'
import { createVuetify } from 'vuetify'
import * as components from 'vuetify/components'
import * as directives from 'vuetify/directives'
import axios from "axios";

const vuetify = createVuetify({
    components,
    directives,
})

const token = localStorage.getItem('userToken')
export const axiosInstance = axios.create({
    baseURL: 'http://localhost:3001/',
    headers: {'token': token}
});

axiosInstance.defaults.withCredentials = true

if (token) {
    await store.dispatch('user/getUser')
}

// Redirect to login if I get unauthorized
axiosInstance.interceptors.response.use(response => {
    return response
}, error => {
    if (error.response?.status === 403 || error.response?.status === 401) {
        router.push('/login')
    }
    return Promise.reject(error)
})

const app = createApp(App).use(vuetify)

app.use(router)
app.use(store)

app.mount('#app')
