import { createRouter, createWebHistory } from 'vue-router'
const router = createRouter({
    history: createWebHistory(import.meta.env.BASE_URL),
    routes: [
        {
            path: '/login',
            name: 'login',
            component: () => import('../views/LoginView.vue')
        },
        { 
          path: '/auth/callback',
          name: 'callback',
          component: () => import('../views/AuthCallback.vue') 
        },
        { 
          path: '/dashboard',
          name: 'dashboard',
          component: () => import('../views/DashboardView.vue') 
        },
        {
            path: '/',
            name: '',
            component: () => import('../views/MainLayout.vue'),
            redirect: '/subjects',
            children: [
                {
                    path: 'subjects',
                    name: 'subjects',
                    component: () => import('../views/Subjects/Subjects.vue'),
                    children: [
                        {
                            path: '',
                            name: 'subjects-list',
                            component: () => import('../views/Subjects/SubjectsList.vue')
                        },
                        {
                            path: 'add',
                            name: 'subjects-add',
                            component: () => import('../views/Subjects/SubjectsAdd.vue')
                        },
                        {
                            path: 'edit/:id',
                            name: 'subjects-edit',
                            component: () => import('../views/Subjects/SubjectsAdd.vue')
                        },
                        {
                            path: ':id',
                            name: 'subjects-item',
                            component: () => import('../views/Subjects/SubjectsItem.vue')
                        },
                    ]
                },
                {
                    path: 'users',
                    name: 'users',
                    component: () => import('../views/Users/Users.vue'),
                    children: [
                        {
                            path: '',
                            name: 'users-list',
                            component: () => import('../views/Users/UsersList.vue')
                        },
                        {
                            path: 'add',
                            name: 'users-add',
                            component: () => import('../views/Users/UsersAdd.vue')
                        },
                    ]
                }
            ]
        },
    ]
})

export default router
