import { axiosInstance } from '@/main'
import router from '@/router'
import { serverUrl } from '@/utils/constants.js'

export const user = {
  namespaced: true,
  state() {
    return {
      loggedIn: false,
      user: null
    }
  },
  actions: {
    async getUser({ commit }) {
      try {
        const response = await axiosInstance.get(`auth/loggedUser`)
        commit('setUser', response.data)
        commit('setLoggedIn', true)
      } catch (error) {
        console.error('Error fetching user:', error)
        commit('setLoggedIn', false)
      }
    },

    async checkLogin({ commit }) {
      try {
        const response = await axiosInstance.get(`auth/logged`)
        const token = response.data.token

        if (token) {
          localStorage.setItem('userToken', token)
          axiosInstance.interceptors.request.use(config => {
            config.headers['token'] = token
            return config;
          }, error => {
              return Promise.reject(error);
          })
        }

        commit('setUser', response.data.user)
        commit('setLoggedIn', response.data.loggedIn)
        router.push('/')
      } catch (error) {
        console.error('Error fetching user:', error)
        commit('setLoggedIn', false)
      }
    },

    async googleLogin({ commit }) {
      try {
        const response = await axiosInstance.get(`auth/url`)
        window.location.href = response.data.url
      } catch (err) {
        console.error(err)
      }
    },

    async login({ commit }, form) {
      try {
        const response = await axiosInstance.post(`auth/login`, form)
        
        const token = response.data.token
        if (token) {
          localStorage.setItem('userToken', token)
          axiosInstance.interceptors.request.use(config => {
            config.headers['token'] = token
            return config;
          }, error => {
              return Promise.reject(error);
          })
        }
        router.push('/')
        commit('setUser', response.data)

      } catch (err) {
        console.error(err)
        commit('setLoggedIn', false)
        router.push('/login')
      }
    },

    async getUsers({ commit }) {
      try {
        const response = await axiosInstance.get(`user`)
        commit('setUser', response.data)
        commit('setLoggedIn', true)
      } catch (error) {
        console.error(error)
        commit('setLoggedIn', false)
        router.push('/login')
      }
    },
  },
  mutations: {
    setUser(state, user) {
      state.user = user
    },
    setLoggedIn(state, status) {
      state.loggedIn = status
    }
  },
  getters: {
    isLogged(state) {
      return state.loggedIn
    },
    user(state) {
      return state.user
    }
  }
}