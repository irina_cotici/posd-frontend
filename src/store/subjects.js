import { axiosInstance } from '@/main'
import { serverUrl } from '@/utils/constants.js'

export const subjects = {
  namespaced: true,
  state() {
    return {
      subjects: [],
      tasks: [],
      item: {},
      task: {},
    }
  },
  actions: {
    async getSubjects({ commit }) {
      try {
        const response = await axiosInstance.get(`subject`)
        commit('setSubjects', response.data)
      } catch (error) {
        console.error(error)
      }
    },

    async getSubject({ commit }, id) {
      try {
        const response = await axiosInstance.get(`subject/${id}`)
        commit('setItem', response.data)
        console.log(response.data.assignments)
        commit('setTasks', response.data.assignments)
      } catch (error) {
        console.error(error);
      }
    },

    async getTask({ commit }, id) {
      try {
        const response = await axiosInstance.get(`assignment/${id}`)
        const task = {id: id, ...response.data}
        commit('setTask', task)
      } catch (error) {
        console.error(error);
      }
    },

    async removeTask({ commit }, id) {
      try {
        const response = await axiosInstance.delete(`assignment/${id}`)
      } catch (error) {
        console.error(error);
      }
    },

    async editTask({ commit }, id) {
      try {
        const response = await axiosInstance.get(`assignment/${id}`)
        const task = {id: id, ...response.data}
        commit('setTask', task)
      } catch (error) {
        console.error(error);
      }
    },

    async submitAssignment({ commit }, { id, assignment }) {
      try {
        const response = await axiosInstance.post(`/assignment/${id}`, {
          type: assignment.type,
          question: assignment.question,
          variants: assignment.variants
        });

        commit('addTask', response.data.id);
      } catch (e) {
        console.error('Error submitting assignment', e);
      }
    },
  },
  mutations: {
    setSubjects(state, subjects) {
      state.subjects = subjects
    },
    addTask(state, task) {
      state.tasks.push(task)
    },
    setTasks(state, task) {
      state.tasks = task
    },
    setItem(state, item) {
      state.item = item
    },
    setTask(state, item) {
      state.task = item
    }
  },
  getters: {
    subjects(state) {
      return state.subjects
    },
    tasks(state) {
      return state.tasks
    },
    item(state) {
      return state.item
    },
    task(state) {
      return state.task
    }
  }
}