import { createStore } from 'vuex'
import { user } from './user'
import { subjects } from './subjects'

export default createStore({
  modules: {
    user,
    subjects
  },
})